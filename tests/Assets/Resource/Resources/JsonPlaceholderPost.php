<?php

namespace Tests\Assets\Resource\Resources;

use Helium\ApiWrapper\Resource\Contracts\All as AllContract;
use Helium\ApiWrapper\Resource\Contracts\Create as CreateContract;
use Helium\ApiWrapper\Resource\Contracts\Delete as DeleteContract;
use Helium\ApiWrapper\Resource\Contracts\Get as GetContract;
use Helium\ApiWrapper\Resource\Contracts\Update as UpdateContract;
use Helium\ApiWrapper\Resource\Operations\All;
use Helium\ApiWrapper\Resource\Operations\Create;
use Helium\ApiWrapper\Resource\Operations\Delete;
use Helium\ApiWrapper\Resource\Operations\Get;
use Helium\ApiWrapper\Resource\Operations\Update;
use Helium\ApiWrapper\Resource\ApiResource;

/**
 * Class JsonPlaceholderPost
 * @property string id
 * @property string userId
 * @property string title
 * @property bool completed
 */
class JsonPlaceholderPost extends ApiResource implements AllContract, CreateContract, DeleteContract, GetContract, UpdateContract
{
    use All;
    use Create;
    use Delete;
    use Get;
    use Update;

    protected $allRoute = 'posts.all';
    protected $createRoute = 'posts.create';
    protected $deleteRoute = 'posts.delete';
    protected $getRoute = 'posts.get';
    protected $updateRoute = 'posts.update';
}